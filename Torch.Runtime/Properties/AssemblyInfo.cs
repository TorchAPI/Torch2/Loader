﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Torch.Runtime")]
[assembly: AssemblyDescription("Default plugin runtime for the Torch Loader")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Torch API")]
[assembly: AssemblyProduct("Torch.Runtime")]
[assembly: AssemblyCopyright("Copyright ©  2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("58A6A553-1901-48F3-A0C9-86F102CD1867")]

[assembly: AssemblyVersion("0.1.0")]
[assembly: AssemblyFileVersion("0.1.0")]
[assembly: AssemblyInformationalVersion("0.1.0")]