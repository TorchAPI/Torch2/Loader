﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using NLog;
using Torch.Core.Plugin;
using Torch.Core.Util;

namespace Torch.Runtime
{
    public class PluginRuntime : IPluginRuntime
    {
        private static readonly ILogger _log = LogManager.GetCurrentClassLogger();
        
        internal PluginRuntime(RuntimeBuilder builder)
        {
            builder.Create(out var packages, out var frameworks, out var plugins);
            var pluginDictionary = new Dictionary<Type, IPlugin>();
            var pluginToPackage = new Dictionary<IPlugin, IPackage>();
            {
                var bag = new TypedBag<IPlugin>();
                foreach (var k in plugins)
                    bag.Add(k);
                bag.Flatten(pluginDictionary);
            }
            {
                foreach (var pkg in packages.Values)
                foreach (var plug in pkg.Plugins)
                    pluginToPackage.Add(plug, pkg);
            }
            _plugins = pluginDictionary;
            Plugins = new ReadOnlyList<IPlugin>(plugins);
            Packages = new ReadOnlyDictionary<string, IPackage>(packages);
            Frameworks = new ReadOnlyDictionary<string, IFramework>(frameworks);
            _pluginToPackage = pluginToPackage;

            foreach (var pkg in Packages.Values)
            {
                _log.Info("Package: " + pkg.Id + "/" + pkg.Version + "\tPlugins=" +
                                  string.Join(", ", pkg.Plugins.Select(x => x.GetType().FullName)) + "\tDependencies=" +
                                  string.Join(", ", pkg.Dependencies.Select(x => x.Id)) + "\tDependents=" +
                                  string.Join(", ", pkg.Dependents.Select(x => x.Id)));
            }

            foreach (var fra in Frameworks.Values)
            {
                _log.Info("Framework: " + fra.Provided.Id + " in " + fra.InstallDirectory);
            }
        }

        public IReadOnlyDictionary<string, IPackage> Packages { get; }
        public IReadOnlyDictionary<string, IFramework> Frameworks { get; }

        private readonly IReadOnlyDictionary<Type, IPlugin> _plugins;
        private readonly IReadOnlyDictionary<IPlugin, IPackage> _pluginToPackage;

        public IPackage PackageFor(IPlugin plugin) => _pluginToPackage[plugin];

        public IReadOnlyList<IPlugin> Plugins { get; }

        public T GetPlugin<T>() where T : IPlugin
        {
            return (T) GetPlugin(typeof(T));
        }

        public IPlugin GetPlugin(Type t)
        {
            return _plugins.TryGetValue(t, out var res) ? res : null;
        }

        public RuntimeState State { get; private set; } = RuntimeState.Stopped;

        private readonly AutoResetEvent _stateChanged = new AutoResetEvent(false);

        internal void Start()
        {
            State = RuntimeState.Starting;
            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < Plugins.Count; i++)
                Plugins[i].InitEarly(this);
            for (var i = Plugins.Count - 1; i >= 0; i--)
                Plugins[i].InitLate();
            State = RuntimeState.Running;
            while (State == RuntimeState.Running)
            {
                _stateChanged.WaitOne();
            }

            for (var i = Plugins.Count - 1; i >= 0; i--)
                Plugins[i].DisposeEarly();
            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < Plugins.Count; i++)
                Plugins[i].DisposeLate();
            State = RuntimeState.Stopped;
        }

        public void Stop()
        {
            State = RuntimeState.Stopping;
            _stateChanged.Set();
        }
    }
}