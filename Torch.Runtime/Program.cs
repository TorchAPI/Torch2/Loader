﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using NLog;
using NLog.Config;
using NLog.Targets;
using Torch.Core.Plugin;

namespace Torch.Runtime
{
    public static class Program
    {
        private static readonly ILogger _log = LogManager.GetCurrentClassLogger();

        public static void Main(string[] args)
        {
            if (LogManager.Configuration == null)
            {
                var cfg = new LoggingConfiguration();
                cfg.AddTarget(new ColoredConsoleTarget("default"));
                cfg.AddRuleForAllLevels("default");
                LogManager.Configuration = cfg;
            }

            Startup();
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        private static void Startup()
        {
            RuntimeBuilder runtimeCfg;

            // ReSharper disable once AssignNullToNotNullAttribute
            using (var stream = new MemoryStream(Convert.FromBase64String(Environment.GetEnvironmentVariable("Torch.Runtime"))))
            {
                var binfmt = new BinaryFormatter();
                runtimeCfg = (RuntimeBuilder) binfmt.Deserialize(stream);
            }

            new PluginRuntime(runtimeCfg).Start();
        }
    }
}