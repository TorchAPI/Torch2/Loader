﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Xml.Serialization;
using NLog;
using NLog.Config;
using NLog.Fluent;
using NLog.Targets;
using NuGet.Runtime;

namespace Torch.Loader.Console
{
    internal static class Program
    {
        public static void Main(string[] args)
        {
            string runtimeData;
            string runtimeFile;
            {
                var domain = AppDomain.CreateDomain("Launcher");
                var launch = domain.CreateInstance<CrossDomainLaunch>();
                runtimeData = launch.RuntimeData;
                runtimeFile = launch.RuntimeExe;
                AppDomain.Unload(domain);
            }

#if DEBUG
            {
                // Debug mode = launch in same app domain so debugger is attachable.
                Environment.SetEnvironmentVariable(LoaderContext.RuntimeEnvKey, runtimeData);
#pragma warning disable 618
                AppDomain.CurrentDomain.AppendPrivatePath(Path.GetDirectoryName(runtimeFile));
#pragma warning restore 618
                AppDomain.CurrentDomain.ExecuteAssembly(runtimeFile);
            }
#else
            {
                // Release mode = launch in separate process
                var psi = new ProcessStartInfo();
                psi.Environment[LoaderContext.RuntimeEnvKey] = runtimeData;
                psi.UseShellExecute = false;
                psi.FileName = runtimeFile;
                var process = Process.Start(psi);
                process.WaitForExit();
                //Environment.Exit(0);
            }
#endif
        }

        // ReSharper disable once ClassNeverInstantiated.Local
        private class CrossDomainLaunch : MarshalByRefObject
        {
            public readonly string RuntimeData;
            public readonly string RuntimeExe;

            public CrossDomainLaunch()
            {
                if (LogManager.Configuration == null) {
                    var cfg = new LoggingConfiguration();
                    cfg.AddTarget(new ColoredConsoleTarget("default"));
                    cfg.AddRuleForAllLevels("default");
                    LogManager.Configuration = cfg;
                }

                var ctx = new LoaderContext("./instance/");
                ctx.Load();
                ctx.Patch();
                var info = ctx.Launch();
                RuntimeData = info.Environment[LoaderContext.RuntimeEnvKey];
                RuntimeExe = info.FileName;
            }
        }
    }
}