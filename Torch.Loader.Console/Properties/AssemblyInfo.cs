﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Torch.Loader.Console")]
[assembly: AssemblyDescription("Console based interface to the Torch Loader System")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Torch API")]
[assembly: AssemblyProduct("Torch.Loader.Console")]
[assembly: AssemblyCopyright("Copyright ©  2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("A89293BE-C7FB-4259-B9A1-1441A8B8C803")]

[assembly: AssemblyVersion("0.2.0")]
[assembly: AssemblyFileVersion("0.2.0")]
[assembly: AssemblyInformationalVersion("0.2.0-dev")]