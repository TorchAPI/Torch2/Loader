﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Torch.Core")]
[assembly: AssemblyDescription("Core components of the Torch Loader")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Torch API")]
[assembly: AssemblyProduct("Torch.Core")]
[assembly: AssemblyCopyright("Copyright ©  2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("762B1528-CAC2-44B1-B0BC-1D10D56D6939")]

[assembly: AssemblyVersion("0.1.0")]
[assembly: AssemblyFileVersion("0.1.0")]
[assembly: AssemblyInformationalVersion("0.1.0")] 