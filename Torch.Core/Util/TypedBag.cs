﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading;

namespace Torch.Core.Util
{
    /// <summary>
    /// Dictionary accelerated Type -> Single Object Of Type query.  Use <see cref="Flatten"/> if it won't change during runtime.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class TypedBag<T> where T : class
    {
        private readonly Dictionary<Type, HashSet<T>> _entries = new Dictionary<Type, HashSet<T>>();
        private readonly ReaderWriterLockSlim _lock = new ReaderWriterLockSlim();

        public event Action<Type, T> Added, Removed;

        public void Add(T obj)
        {
            Add(obj.GetType(), obj);
        }

        public void Remove(T obj)
        {
            Remove(obj.GetType(), obj);
        }

        /// <summary>
        /// Flattens this typed bag into the given dictionary.
        /// </summary>
        /// <param name="dest">Destination dictionary</param>
        public void Flatten(Dictionary<Type, T> dest)
        {
            try
            {
                _lock.EnterReadLock();
                foreach (var kv in _entries)
                    if (kv.Value.Count == 1)
                        dest.Add(kv.Key, kv.Value.First());
            }
            finally
            {
                _lock.ExitReadLock();
            }
        }

        /// <summary>
        /// Gets the element assignable to the given type, or null if there is not <b>one</b> such element.
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public T Get(Type t)
        {
            try
            {
                _lock.EnterReadLock();
                if (!_entries.TryGetValue(t, out var set))
                    return null;
                return set.Count == 1 ? set.First() : null;
            }
            finally
            {
                _lock.ExitReadLock();
            }
        }

        /// <summary>
        /// Gets the element assignable to the given type, or null if there is not <b>one</b> such element.
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public TV GetAs<TV>() where TV : T
        {
            return (TV) Get(typeof(TV));
        }

        private void Add(Type ft, T obj, bool skipInterfaces = false)
        {
            while (ft != null)
            {
                bool added;
                T evicted = null;
                try
                {
                    _lock.EnterWriteLock();

                    if (!_entries.TryGetValue(ft, out var set))
                        _entries.Add(ft, set = new HashSet<T>());
                    if (set.Count == 1)
                        evicted = set.First();
                    added = set.Add(obj);
                }
                finally
                {
                    _lock.ExitWriteLock();
                }

                if (added)
                {
                    if (evicted == null)
                        Added?.Invoke(ft, obj);
                    else
                        Removed?.Invoke(ft, evicted);
                }


                if (!skipInterfaces)
                    foreach (var inter in ft.GetInterfaces())
                        Add(inter, obj, true);

                ft = ft.BaseType;
                skipInterfaces = true;
            }
        }

        private void Remove(Type ft, T obj, bool skipInterfaces = false)
        {
            while (ft != null)
            {
                bool removed = false;
                T remains = null;
                try
                {
                    _lock.EnterWriteLock();

                    if (_entries.TryGetValue(ft, out var set))
                    {
                        removed = set.Remove(obj);
                        if (set.Count == 1)
                            remains = set.First();
                        if (set.Count == 0)
                            _entries.Remove(ft);
                    }
                }
                finally
                {
                    _lock.ExitWriteLock();
                }

                if (removed)
                {
                    if (remains == null)
                        Removed?.Invoke(ft, obj);
                    else
                        Added?.Invoke(ft, remains);
                }

                if (!skipInterfaces)
                    foreach (var inter in ft.GetInterfaces())
                        Remove(inter, obj, true);

                ft = ft.BaseType;
                skipInterfaces = true;
            }
        }
    }
}