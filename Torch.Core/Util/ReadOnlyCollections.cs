﻿using System.Collections;
using System.Collections.Generic;

namespace Torch.Core.Util
{
    public class ReadOnlyList<T> : IReadOnlyList<T>
    {
        private readonly IReadOnlyList<T> _backing;

        public ReadOnlyList(IReadOnlyList<T> backing)
        {
            _backing = backing;
        }

        public IEnumerator<T> GetEnumerator() => _backing.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable) _backing).GetEnumerator();

        public int Count => _backing.Count;

        public T this[int index] => _backing[index];
    }

    public class ReadOnlyDictionary<TK, TV> : IReadOnlyDictionary<TK, TV>
    {
        private readonly IReadOnlyDictionary<TK, TV> _backing;

        public ReadOnlyDictionary(IReadOnlyDictionary<TK, TV> backing)
        {
            _backing = backing;
        }
            
        public IEnumerator<KeyValuePair<TK, TV>> GetEnumerator() => _backing.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable) _backing).GetEnumerator();

        public int Count => _backing.Count;

        public bool ContainsKey(TK key) => _backing.ContainsKey(key);

        public bool TryGetValue(TK key, out TV value) => _backing.TryGetValue(key, out value);

        public TV this[TK key] => _backing[key];

        public IEnumerable<TK> Keys => _backing.Keys;

        public IEnumerable<TV> Values => _backing.Values;
    }
}