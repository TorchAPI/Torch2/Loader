﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using NLog;

namespace Torch.Core.Util
{
    public static class DagSorter
    {
        private static readonly ILogger _log = LogManager.GetCurrentClassLogger();

        public static IEnumerable<T> SortDag<T>(this IEnumerable<T> input, Func<T, IEnumerable<T>> incoming = null,
            Func<T, float> priority = null)
        {
            Dictionary<T, WorkData<T>> work = new Dictionary<T, WorkData<T>>();
            foreach (var k in input)
                work.Add(k, new WorkData<T>(k, priority?.Invoke(k) ?? 0f));

            if (incoming != null)
                foreach (var kv in work)
                {
                    foreach (var dep in incoming(kv.Key))
                    {
                        if (!work.TryGetValue(dep, out var other))
                            continue;
                        kv.Value.Incoming.Add(dep);
                        other.Outgoing.Add(kv.Key);
                        kv.Value.UnsolvedIncoming++;
                    }
                }

            var unsolved = new List<WorkData<T>>(work.Values);
            var solved = new List<T>(work.Count);
            while (unsolved.Count > 0)
            {
                unsolved.Sort(WorkData<T>.Comparer);
                var best = unsolved[0];
                if (best.UnsolvedIncoming > 0)
                {
                    foreach (WorkData<T> manager in unsolved)
                    {
                        _log.Fatal("   + {0} has {1} incoming left.", manager.Value, manager.UnsolvedIncoming);
                        _log.Fatal("        - Incoming: {0}",
                            string.Join(", ",
                                manager.Incoming.Select(x => ((object) x) == null ? "null" : x.ToString())));
                        _log.Fatal("        - Outgoing: {0}",
                            string.Join(", ",
                                manager.Outgoing.Select(x => ((object) x) == null ? "null" : x.ToString())));
                    }

                   throw new InvalidOperationException($"Failed to solve DAG");
                }

                solved.Add(best.Value);
                unsolved.RemoveAt(0);
                foreach (var k in best.Outgoing)
                    work[k].UnsolvedIncoming--;
            }

            return solved;
        }


        private class WorkData<T>
        {
            public readonly T Value;
            public readonly HashSet<T> Incoming = new HashSet<T>();
            public readonly HashSet<T> Outgoing = new HashSet<T>();

            // ReSharper disable once MemberCanBePrivate.Local
            public readonly float Priority;
            public int UnsolvedIncoming;

            public WorkData(T val, float prior)
            {
                Value = val;
                Priority = prior;
            }

            public static readonly Comparison<WorkData<T>> Comparer = Compare;

            private static int Compare(WorkData<T> a, WorkData<T> b)
            {
                // low to high
                if (a.UnsolvedIncoming != b.UnsolvedIncoming)
                    return a.UnsolvedIncoming.CompareTo(b.UnsolvedIncoming);
                // high to low
                return -a.Priority.CompareTo(b.Priority);
            }
        }
    }
}