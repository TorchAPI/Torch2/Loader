﻿using System;
using System.Reflection;

namespace Torch.Core.Util
{
    /// <summary>
    /// Controls the priority of an item.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class PriorityAttribute : Attribute
    {
        /// <summary>
        /// Higher priority things must come before lower priority things.
        /// </summary>
        public float Priority { get; set; } = 0;
    }
}