﻿using System;

namespace Torch.Core.Util
{
    /// <summary>
    /// If something *must* occur before another thing use this.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class DependencyAttribute : Attribute
    {
        public DependencyAttribute(Type dependency)
        {
            Dependency = dependency;
        }

        /// <summary>
        /// Type this depends on
        /// </summary>
        public Type Dependency { get; }
    }
}