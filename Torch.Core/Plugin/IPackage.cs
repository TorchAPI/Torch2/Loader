﻿using System.Collections.Generic;

namespace Torch.Core.Plugin
{
    public interface IPackage
    {
        string Id { get; }
        
        string Version { get; }
        
        IEnumerable<IPlugin> Plugins { get; }
        
        IEnumerable<IPackage> Dependencies { get; }
        
        IEnumerable<IPackage> Dependents { get; }
        
        string InstallPath { get; }
    }
}