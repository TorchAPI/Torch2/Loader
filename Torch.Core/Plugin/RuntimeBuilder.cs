﻿using System;
using System.Collections.Generic;
using Torch.Core.Util;

namespace Torch.Core.Plugin
{
    [Serializable]
    public class RuntimeBuilder
    {
        [Serializable]
        public class PackageBuilder
        {
            public string Name;
            public string Version;
            public string InstallPath;
            public string[] Dependencies;
        }

        [Serializable]
        public class FrameworkBuilder
        {
            public string Provided;
            public string InstallDirectory;
        }

        [Serializable]
        public class PluginBuilder
        {
            public string Package;
            public string PluginType;
        }

        public PluginBuilder[] Plugins;
        public PackageBuilder[] Packages;
        public FrameworkBuilder[] Frameworks;

        public void Create(out Dictionary<string, IPackage> packages, out Dictionary<string, IFramework> frameworks,
            out List<IPlugin> plugins)
        {
            packages = new Dictionary<string, IPackage>();
            if (Packages != null)
            {
                foreach (var pkg in Packages)
                    packages.Add(pkg.Name, new Package(pkg.Name, pkg.Version, pkg.InstallPath));

                foreach (var pkg in Packages)
                    if (pkg.Dependencies != null)
                    {
                        var self = ((Package) packages[pkg.Name]);
                        foreach (var dep in pkg.Dependencies)
                        {
                            var other = (Package) packages[dep];
                            self.Dependencies.Add(other);
                            other.Dependents.Add(self);
                        }
                    }
            }

            frameworks = new Dictionary<string, IFramework>();
            if (Frameworks != null)
            {
                foreach (var fra in Frameworks)
                    frameworks.Add(fra.Provided, new Framework(fra.InstallDirectory, packages[fra.Provided]));
            }

            plugins = new List<IPlugin>();
            // ReSharper disable once InvertIf
            if (Plugins != null)
            {
                foreach (var plug in Plugins)
                {
                    var type = Type.GetType(plug.PluginType, false);
                    if (type == null)
                        throw new InvalidOperationException($"Failed to load type \"{plug.PluginType}\"");
                    // ReSharper disable once UsePatternMatching
                    var plugin = Activator.CreateInstance(type) as IPlugin;
                    if (plugin == null)
                        throw new InvalidOperationException(
                            $"Type {type.AssemblyQualifiedName} failed to cast to plugin");

                    plugins.Add(plugin);
                    ((Package) packages[plug.Package]).Plugins.Add(plugin);
                }
            }
        }

        private class Package : IPackage
        {
            public readonly List<IPlugin> Plugins;
            public readonly List<IPackage> Dependencies, Dependents;

            private readonly IReadOnlyList<IPlugin> _pluginsRo;
            private readonly IReadOnlyList<IPackage> _dependenciesRo, _dependentsRo;

            public Package(string id, string version, string installPath)
            {
                Id = id;
                Version = version;
                InstallPath = installPath;
                Plugins = new List<IPlugin>();
                Dependencies = new List<IPackage>();
                Dependents = new List<IPackage>();

                _pluginsRo = new ReadOnlyList<IPlugin>(Plugins);
                _dependenciesRo = new ReadOnlyList<IPackage>(Dependencies);
                _dependentsRo = new ReadOnlyList<IPackage>(Dependents);
            }

            public string Id { get; }
            public string Version { get; }
            public string InstallPath { get; }

            IEnumerable<IPlugin> IPackage.Plugins => _pluginsRo;
            IEnumerable<IPackage> IPackage.Dependencies => _dependenciesRo;
            IEnumerable<IPackage> IPackage.Dependents => _dependentsRo;
        }

        private class Framework : IFramework
        {
            public Framework(string installDirectory, IPackage provided)
            {
                InstallDirectory = installDirectory;
                Provided = provided;
            }

            public string InstallDirectory { get; }
            public IPackage Provided { get; }
        }
    }
}