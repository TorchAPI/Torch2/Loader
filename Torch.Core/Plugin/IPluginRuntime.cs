﻿using System;
using System.Collections.Generic;

namespace Torch.Core.Plugin
{
    public interface IPluginRuntime
    {
        IReadOnlyDictionary<string, IPackage> Packages { get; }
        
        IReadOnlyDictionary<string, IFramework> Frameworks { get; }

        IPackage PackageFor(IPlugin plugin);
        
        IReadOnlyList<IPlugin> Plugins { get; }
        
        T GetPlugin<T>() where T : IPlugin;
        
        IPlugin GetPlugin(Type t);

        RuntimeState State { get; }
        
        void Stop();
    }
}