﻿namespace Torch.Core.Plugin
{
    public enum RuntimeState
    {
        Stopped, Starting, Running, Stopping
    }
}