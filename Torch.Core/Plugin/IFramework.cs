﻿namespace Torch.Core.Plugin
{
    public interface IFramework
    {
        string InstallDirectory { get; }
        IPackage Provided { get; }
    }
}