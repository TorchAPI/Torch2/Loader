﻿namespace Torch.Core.Plugin
{
    public interface IPlugin
    {
        /// <summary>
        /// Called during initialization.  Dependencies, then dependents.
        /// </summary>
        /// <param name="runtime"></param>
        /// <remarks>
        /// This is primarily used to register things with a dependency.
        /// </remarks>
        void InitEarly(IPluginRuntime runtime);

        /// <summary>
        /// Called during initialization.  Depdenents, then dependencies.
        /// </summary>
        /// <remarks>
        /// This is primarily used to process anything a dependent has registered.
        /// </remarks>
        void InitLate();

        /// <summary>
        /// Called during shutdown.  Depdenents, then dependencies.
        /// </summary>
        /// <remarks>
        /// This is primarily used to unregister this plugin with its dependencies.
        /// </remarks>
        void DisposeEarly();

        /// <summary>
        /// Called during shutdown.  Dependencies, then dependents.
        /// </summary>
        /// <remarks>
        /// This is primarily used for freeing any "native" resources that may still be in use by a dependency.
        /// </remarks>
        void DisposeLate();
    }
}