﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Torch.Loader")]
[assembly: AssemblyDescription("Loader library for the Torch Loader System")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Torch API")]
[assembly: AssemblyProduct("Torch.Loader")]
[assembly: AssemblyCopyright("Copyright ©  2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("FA813A9A-6256-46DF-B04C-84B5B98F89FD")]

[assembly: AssemblyVersion("0.2.0")]
[assembly: AssemblyFileVersion("0.2.0")]
[assembly: AssemblyInformationalVersion("0.2.0-dev")]