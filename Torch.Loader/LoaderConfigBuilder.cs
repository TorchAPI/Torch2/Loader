﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace Torch.Loader
{
    [XmlType("config")]
    public class LoaderConfigBuilder
    {
        [XmlElement("repository")]
        public string[] Repositories;

        [XmlElement("loader")]
        public PackageReference[] Loader;

        [XmlElement("runtime")]
        public PackageReference Runtime;

        [XmlElement("plugin")]
        public PackageReference[] Plugins;

        public class PackageReference
        {
            [XmlAttribute("id")] public string Id;

            [XmlAttribute("version")] public string Version;

            [XmlAttribute("dev")]
            [DefaultValue(false)]
            public bool Development;

            [XmlAttribute("reinstall")]
            [DefaultValue(false)]
            public bool Reinstall;
        }

        private static readonly XmlSerializer Serializer = new XmlSerializer(typeof(LoaderConfigBuilder));

        public static LoaderConfigBuilder Read(string path)
        {
            using (var stream = File.OpenRead(path))
                return (LoaderConfigBuilder) Serializer.Deserialize(stream);
        }
    }
}