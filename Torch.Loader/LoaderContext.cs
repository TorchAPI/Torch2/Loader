﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Versioning;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using Mono.Cecil;
using NLog;
using NuGet;
using Torch.Core.Plugin;
using Torch.Core.Util;
using Torch.Loader.Plugins.Framework;
using Torch.Loader.Plugins.Patching;
using Torch.Loader.Utils;
using IPackage = NuGet.IPackage;

namespace Torch.Loader
{
    public class LoaderContext
    {
        private static readonly NLog.ILogger _log = LogManager.GetCurrentClassLogger();

        private readonly string _instancePath, _installPath, _syntheticPath;
        private readonly IPackageRepository _syntheticRepository;
        private readonly IPackageRepository _sourceRepository;
        private readonly PackageManager _pluginManager;

        private readonly IReadOnlyList<IPackage> _loaderPlugins;
        private readonly IReadOnlyList<IPackage> _plugins;
        private readonly IPackage _runtime;

        public LoaderContext(string instancePath)
        {
            _instancePath = instancePath;
            _installPath = Path.Combine(_instancePath, "install");
            _syntheticPath = Path.Combine(_instancePath, "synthetic");
            _syntheticRepository = new ExpandedPackageRepository(new PhysicalFileSystem(_syntheticPath));

            var config = LoaderConfigBuilder.Read(Path.Combine(instancePath, "torch.cfg"));
            if (config.Repositories == null || config.Repositories.Length == 0)
                throw new InvalidOperationException($"You have no repositories configured.");
            if (config.Runtime == null)
                throw new InvalidOperationException($"You have no runtime configured.");
            _sourceRepository =
                new AggregateRepository(config.Repositories.Select(PackageRepositoryFactory.Default.CreateRepository));

            _pluginManager = new PackageManager(_sourceRepository, Path.Combine(_instancePath, "plugins"));

            _log.Info("Resolving loader plugins");
            _loaderPlugins = (config.Loader ?? Enumerable.Empty<LoaderConfigBuilder.PackageReference>())
                .Select(ResolvePackage).ToList();
            _log.Info("Resolving remaining plugins");
            _plugins = (config.Plugins ?? Enumerable.Empty<LoaderConfigBuilder.PackageReference>())
                .Select(ResolvePackage).ToList();
            _log.Info("Resolving runtime");
            _runtime = ResolvePackage(config.Runtime);
        }

        private IPackage ResolvePackage(LoaderConfigBuilder.PackageReference pkg)
        {
            IEnumerable<IPackage> query = _sourceRepository.FindPackagesById(pkg.Id);
            var releaseOnly = !pkg.Development;

            if (!string.IsNullOrWhiteSpace(pkg.Version) && !pkg.Version.Equals("latest"))
            {
                var constraint = VersionUtility.ParseVersionSpec(pkg.Version);
                if (constraint.MinVersion != null && !string.IsNullOrEmpty(constraint.MinVersion.SpecialVersion))
                    releaseOnly = false;
                if (constraint.MaxVersion != null && !string.IsNullOrEmpty(constraint.MaxVersion.SpecialVersion))
                    releaseOnly = false;
                query = query.FindByVersion(constraint);
            }

            if (releaseOnly)
                query = query.Where(x => x.IsReleaseVersion());


            IPackage best = null;
            foreach (var test in query)
            {
                if (best == null || test.Version > best.Version)
                    best = test;
            }

            if (best == null)
                throw new FileNotFoundException(
                    $"Couldn't find {pkg.Id} version {pkg.Version ?? "latest"} in repository");
            if (pkg.Reinstall)
            {
                var extracted = ExtractedDirectory(best);
                if (Directory.Exists(extracted))
                    Directory.Delete(extracted, true);
                _pluginManager.UninstallPackage(best, true);
            }

            return best;
        }

        private string ExtractedDirectory(IPackage pkg)
        {
            return Path.Combine(_pluginManager.FileSystem.Root, _pluginManager.PathResolver.GetPackageDirectory(pkg));
        }

        private readonly List<FrameworkContext> _contexts = new List<FrameworkContext>();

        /// <summary>
        /// Sets the frameworks up (stage 1)
        /// </summary>
        public void Load()
        {
            foreach (var pkg in _loaderPlugins)
                _pluginManager.InstallPackageCurrentFramework(pkg);

            var loadersRef = _pluginManager.LocalRepository.Dependencies(_loaderPlugins).ToArray();

            foreach (var pkg in loadersRef)
                PackageResolver.Add(ExtractedDirectory(pkg), pkg);

            var assembliesToLoad = AssemblyRedirect.RedirectAssemblies(loadersRef.SelectMany((pkg) =>
            {
                var extracted = ExtractedDirectory(pkg);
                if (!Directory.Exists(extracted))
                    return Enumerable.Empty<string>();
                if (!VersionUtility.TryGetCompatibleItems(Extensions.CurrentFramework, pkg.GetFiles(), out var files))
                    throw new InvalidOperationException($"No compatible items for {pkg.Id}/{pkg.Version}");
                return files.Where(y => y.IsBinaryFile()).Select(y => Path.Combine(extracted, y.Path));
            }), true);

            _contexts.Clear();
            foreach (var asmPath in assembliesToLoad)
            {
                // ReSharper disable once AssignNullToNotNullAttribute
                var asm = Assembly.LoadFrom(asmPath);
                foreach (var ptype in asm.GetExportedTypes()
                    .Where(x => typeof(IFrameworkProvider).IsAssignableFrom(x) && !x.IsAbstract))
                {
                    var provider = (IFrameworkProvider) Activator.CreateInstance(ptype);
                    _log.Info("Creating context using " + ptype.Name);
                    switch (provider)
                    {
                        case IManagedFrameworkProvider managed:
                            _contexts.Add(new FrameworkContext(managed,
                                Path.Combine(_installPath, managed.ProvidedPackage)));
                            break;
                        case IUnmanagedFrameworkProvider unmanaged:
                            _contexts.Add(new FrameworkContext(unmanaged));
                            break;
                        default:
                            throw new InvalidOperationException(
                                $"Bad framework provider type {provider.GetType()}");
                    }
                }
            }

            var validSyntheticPackages = new HashSet<KeyValuePair<string, SemanticVersion>>();
            foreach (var installed in _contexts)
            {
                if (installed.IsManaged && installed.NeedsUpdate)
                    installed.Update();
                validSyntheticPackages.Add(
                    new KeyValuePair<string, SemanticVersion>(installed.Name, installed.Version));
                _log.Info("Context registered: " + installed.Name + "\t" + installed.Version);
            }

            // Remove old synthetic package from the active repo (so dependency errors are thrown)
            foreach (var badSynth in _syntheticRepository.GetPackages()
                .Select(x => new KeyValuePair<string, SemanticVersion>(x.Id, x.Version))
                .Where(x => !validSyntheticPackages.Contains(x)))
            {
                var localPkg = _pluginManager.LocalRepository.FindPackage(badSynth.Key, badSynth.Value);
                if (localPkg != null)
                {
                    _pluginManager.UninstallPackage(localPkg, true);
                    _log.Info($"Uninstalling old synthetic package {localPkg}");
                }
            }

            // Create+install synthetic packages
            foreach (var synth in _contexts)
            {
                var existing = _syntheticRepository.FindPackage(synth.Name, synth.Version);
                if (existing == null)
                {
                    // Create package
                    var pkg = new PackageBuilder
                    {
                        Id = synth.Name,
                        Version = synth.Version,
                        Description = $"Package placeholder generated by {synth.Provider.FullName}"
                    };
                    pkg.Files.Add(new MockPackageFile(() => new MemoryStream()) {Path = HASH_FILE});
                    pkg.Authors.Add("Generated");

                    var packagePath = pkg.Id + "." + pkg.Version + NuGet.Constants.PackageExtension;
                    var mock = new MockFileSystem();
                    mock.AddFile(packagePath, pkg.Save);

                    var temp = new OptimizedZipPackage(mock, packagePath);
                    _syntheticRepository.AddPackage(temp);

                    _log.Info($"Creating placeholder package {synth.Name} {synth.Version}");
                    existing = _syntheticRepository.FindPackage(synth.Name, synth.Version);
                }
                else
                {
                    _log.Info($"Reusing placeholder package {synth.Name} {synth.Version}");
                }

                _pluginManager.InstallPackageCurrentFramework(existing);
            }
        }

        /// <summary>
        /// Commits all patches (stage 2)
        /// </summary>
        public void Patch()
        {
            _log.Info($"Installing runtime {_runtime}");
            _pluginManager.InstallPackageCurrentFramework(_runtime);

            // Install plugins
            foreach (var plugin in _plugins)
            {
                _log.Info($"Installing plugin {plugin}");
                _pluginManager.InstallPackageCurrentFramework(plugin);
            }

            var referencedPackages = _pluginManager.LocalRepository.Dependencies(_plugins.Concat(new[] {_runtime})).ToArray();
            var failedFrameworkTest = false;
            foreach (var frameworkRef in referencedPackages.Select(y => new KeyValuePair<IPackage, FrameworkContext>(y, _contexts.FirstOrDefault(z => z.Name == y.Id)))
                .Where(k => k.Value != null))
            {
                if (frameworkRef.Key.Version == frameworkRef.Value.Version) continue;
                _log.Fatal(
                    $"Framework {frameworkRef.Key.Id} / {frameworkRef.Key.Version} is referenced, but you have {frameworkRef.Value.Name} / {frameworkRef.Value.Version} installed");
                foreach (var canidate in referencedPackages)
                foreach (var dep in canidate.GetCompatiblePackageDependencies(Extensions.CurrentFramework))
                    if (dep.Id == frameworkRef.Value.Name && !dep.VersionSpec.Satisfies(frameworkRef.Value.Version))
                        _log.Fatal($"Dependent {canidate.Id} / {canidate.Version} requires {dep.Id} / {dep.VersionSpec}");
                failedFrameworkTest = true;
            }

            if (failedFrameworkTest)
                throw new InvalidOperationException($"Found unsupported packages for the current framework.");

            foreach (var pkg in referencedPackages)
                PackageResolver.Add(ExtractedDirectory(pkg), pkg);

            foreach (var pkg in referencedPackages)
            {
                var installed = _pluginManager.LocalRepository.FindPackage(pkg.Id, pkg.Version);
                foreach (var patcher in installed.GetFiles().Where(x => x.IsPatcher()))
                    PackageResolver.AddPath(ExtractedDirectory(pkg), installed, Path.GetDirectoryName(patcher.Path));
            }

            {
                SHA256 hasher = SHA256.Create();

                // hash all patchers
                string patcherHash;
                using (var concatStream = new ConcatStream())
                {
                    foreach (var plugin in referencedPackages)
                    {
                        var installed = _pluginManager.LocalRepository.FindPackage(plugin.Id, plugin.Version);
                        foreach (var file in installed.GetFiles().Where(x => x.IsPatcher() || x.IsMixin()))
                        {
                            concatStream.Add(file.GetStream());
                        }
                    }

                    patcherHash = Extensions.AsHash(hasher.ComputeHash(concatStream));
                }

                _log.Info($"Patching plugin hash: {patcherHash}");

                // and now hash contexts, and patch if needed
                foreach (var context in _contexts)
                {
                    string contextHash;
                    using (var concatStream = new ConcatStream())
                    {
                        foreach (var asm in context.Assemblies.Where(x => x.AllowPatching))
                            concatStream.Add(asm.GetStream());
                        contextHash = Extensions.AsHash(hasher.ComputeHash(concatStream));
                    }

                    _log.Info($"Framework {context.Name} hash: {contextHash}");

                    var existing = _syntheticRepository.FindPackage(context.Name, context.Version);
                    var desiredHash = patcherHash + "\t" + contextHash;
                    var currentHash = existing?.GetFiles().FirstOrDefault(x =>
                            string.Equals(x.Path, HASH_FILE, StringComparison.OrdinalIgnoreCase))?.GetStream()
                        .ReadToEnd()?.Trim();


                    if (desiredHash != currentHash)
                    {
                        _log.Info($"Patching {context.Name}");
                        var builder = new PatchedPackageBuilder(context);
                        builder.Load();
                        builder.Transform(CreateTransformers());
                        builder.Mixin(referencedPackages.SelectMany(pkg => pkg.GetFiles().Where(y => y.IsMixin())));

                        builder.BeginBuilding();
                        builder.AddModules();
                        builder.AddFile(HASH_FILE, desiredHash);
                        builder.FinishBuilding();

                        builder.Save(_syntheticRepository);

                        _log.Info($"Created patched package {context.Name} {context.Version}");
                        existing = _syntheticRepository.FindPackage(context.Name, context.Version);
                        var extracted = ExtractedDirectory(existing);
                        if (Directory.Exists(extracted))
                            Directory.Delete(extracted, true);
                    }
                    else
                    {
                        _log.Info($"Using cached {context.Name} {context.Version}");
                    }

                    _pluginManager.InstallPackageCurrentFramework(existing);
                }
            }
        }

        private List<IAssemblyTransformer> CreateTransformers()
        {
            var transformers = new List<KeyValuePair<IPackage, Type>>();


            var resolveDirs = new HashSet<string>(AssemblyRedirect.RedirectAssemblies(_plugins.Concat(new[] {_runtime}).SelectMany(
                (pkg) =>
                {
                    var installed = _pluginManager.LocalRepository.FindPackage(pkg.Id, pkg.Version);
                    var extracted = ExtractedDirectory(installed);
                    // ReSharper disable once ConvertIfStatementToReturnStatement
                    if (!Directory.Exists(extracted))
                        return Enumerable.Empty<string>();
                    return installed.GetFiles().Where(y => y.IsBinaryFile() && !y.IsPatcher()).Select(y => Path.Combine(extracted, y.Path));
                }), true).Select(Path.GetDirectoryName));

            var assembliesToLoad = AssemblyRedirect.RedirectAssemblies(_plugins.Concat(new[] {_runtime}).SelectMany(
                (pkg) =>
                {
                    var installed = _pluginManager.LocalRepository.FindPackage(pkg.Id, pkg.Version);
                    var extracted = ExtractedDirectory(installed);
                    if (!Directory.Exists(extracted))
                        return Enumerable.Empty<KeyValuePair<IPackage, string>>();
                    return installed.GetFiles().Where(y => y.IsPatcher()).Select(y =>
                        new KeyValuePair<IPackage, string>(pkg, Path.Combine(extracted, y.Path)));
                }), true);

            foreach (var asmPath in assembliesToLoad)
            {
                // ReSharper disable once AssignNullToNotNullAttribute
                var asm = Assembly.LoadFrom(asmPath.Value);
                foreach (var ptype in asm.GetExportedTypes()
                    .Where(x => typeof(IAssemblyTransformer).IsAssignableFrom(x) && !x.IsAbstract &&
                                !x.IsInterface))
                {
                    transformers.Add(new KeyValuePair<IPackage, Type>(asmPath.Key, ptype));
                }
            }

            IAssemblyResolver resolver;
            {
                var tmp = new DefaultAssemblyResolver();
                resolver = tmp;
                foreach (var path in resolveDirs)
                    tmp.AddSearchDirectory(path);
            }
            return transformers.SelectOrdered().Select(x =>
            {
                var transformer = (IAssemblyTransformer) Activator.CreateInstance(x.Value);
                var replacer = transformer as IAssemblyReplacer;
                replacer?.Init(resolver);
                _log.Info("Adding assembly transformer: " + transformer + (replacer != null ? " [R]" : ""));
                return transformer;
            }).ToList();
        }

        /// <summary>
        /// Launches Torch (stage 3)
        /// </summary>
        /// <returns>info that should be used to spawn the process</returns>
        public ProcessStartInfo Launch()
        {
            var packages = _pluginManager.LocalRepository.Dependencies(_plugins.Select(x => _pluginManager.LocalRepository.FindPackage(x.Id, x.Version))
                    .Concat(_contexts.Select(ctx => _pluginManager.LocalRepository.FindPackage(ctx.Name, ctx.Version))))
                .ToList();
            _log.Info($"Packages: " + string.Join(", ", packages.Select(x => x.Id + "/" + x.Version)));

            List<KeyValuePair<IPackage, Type>> orderedPlugins;

            var assemblies = AssemblyRedirect.RedirectAssemblies(packages.SelectMany(plugin =>
                {
                    if (!VersionUtility.TryGetCompatibleItems(Extensions.CurrentFramework, plugin.GetFiles(), out var libraries))
                        throw new InvalidOperationException($"No compatible items for {plugin.Id}/{plugin.Version}");
                    var extracted = ExtractedDirectory(plugin);
                    if (!Directory.Exists(extracted))
                        throw new InvalidOperationException($"Unable to find extracted plugin");
                    return libraries.Where(x => x.IsBinaryFile() && !x.IsPatcher()).Select(x => new KeyValuePair<IPackage, string>(plugin, Path.Combine(extracted, x.Path)));
                }
            ), false);

            #region Generate Path

            var path = new HashSet<string>();
            foreach (var plugin in packages)
            {
                var extracted = ExtractedDirectory(plugin);
                if (!Directory.Exists(extracted))
                    throw new InvalidOperationException($"Unable to find extracted plugin");
                var natives = Path.Combine(extracted, "lib", "native");
                if (Directory.Exists(natives))
                    path.Add(natives);
                var nativesSpec = Path.Combine(natives, Environment.Is64BitProcess ? "x64" : "x86");
                if (Directory.Exists(nativesSpec))
                    path.Add(nativesSpec);
            }

            foreach (var asm in assemblies)
            {
                path.Add(Path.GetDirectoryName(asm.Value));
            }

            #endregion

            #region Find plugin Types

            {
                foreach (var p in path)
                    PackageResolver.AddPath(p);

                var plugins = new List<KeyValuePair<IPackage, Type>>();
                var resolveTable = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
                foreach (var k in assemblies)
                    // ReSharper disable once AssignNullToNotNullAttribute
                    resolveTable[Path.GetFileNameWithoutExtension(k.Value)] = k.Value;

                Assembly AsmResolver(object sender, ResolveEventArgs args)
                {
                    var asmName = new AssemblyName(args.Name);
                    // ReSharper disable once ConvertIfStatementToReturnStatement
                    if (resolveTable.TryGetValue(asmName.Name, out var resolvePath))
                        return Assembly.ReflectionOnlyLoadFrom(resolvePath);
                    return Assembly.ReflectionOnlyLoad(args.Name);
                }

                AppDomain.CurrentDomain.ReflectionOnlyAssemblyResolve += AsmResolver;
                foreach (var asm in assemblies)
                {
                    bool synthetic = false;
                    foreach (var ctx in _contexts)
                        if (ctx.Name == asm.Key.Id)
                        {
                            synthetic = true;
                            break;
                        }

                    if (synthetic)
                        continue;

                    var asmData = AsmResolver(null, new ResolveEventArgs(Path.GetFileNameWithoutExtension(asm.Value)));
                    foreach (var type in asmData.GetExportedTypes())
                        if (typeof(IPlugin).IsAssignableFromFuzzy(type) && !type.IsAbstract && !type.IsInterface)
                            plugins.Add(new KeyValuePair<IPackage, Type>(asm.Key, type));
                }

                AppDomain.CurrentDomain.ReflectionOnlyAssemblyResolve -= AsmResolver;

                orderedPlugins = plugins.SelectOrdered().ToList();
            }

            #endregion


            #region Find Entry Point

            string exePath;
            {
                var extracted = ExtractedDirectory(_runtime);
                if (!VersionUtility.TryGetCompatibleItems(Extensions.CurrentFramework, _runtime.GetFiles(),
                    out var libraries))
                    throw new InvalidOperationException($"Unable to find any supported runtime {_runtime}");

                var exes = libraries.Where(x => x.Path.EndsWith(".exe", StringComparison.OrdinalIgnoreCase)).ToArray();
                if (exes.Length == 0)
                    throw new InvalidOperationException($"Unable to find entry point for runtime {_runtime}");
                if (exes.Length > 1)
                    throw new InvalidOperationException(
                        $"Found {exes.Length} entry points for runtime {_runtime} ({string.Join(", ", exes.Select(x => x.Path))}");

                exePath = Path.Combine(extracted, exes[0].Path);
                if (!File.Exists(exePath))
                    throw new InvalidOperationException($"Entry point {exePath} wasn't found");
            }

            #endregion

            _log.Info($"Plugins: " + string.Join(", ", orderedPlugins.Select(x => x.Value.FullName)));
            _log.Info($"Path: " + string.Join("; ", path));
            _log.Info($"Entry point: {exePath}");


            ProcessStartInfo psi = new ProcessStartInfo();

            #region Setup Library Path

            var probePath = Path.Combine(_instancePath, "probe");
            var finalExePath = Path.Combine(probePath, Path.GetFileName(exePath));
            FillProbePath(probePath, path.Concat(new[] {exePath}));
            if (!File.Exists(finalExePath))
                throw new InvalidOperationException($"Failed to copy runtime EXE {exePath} to {finalExePath}");

            #endregion

            #region Build Runtime Config

            {
                var builder = new RuntimeBuilder
                {
                    Packages = packages.Select(x => new RuntimeBuilder.PackageBuilder()
                    {
                        Name = x.Id,
                        Version = x.Version.ToFullString(),
                        InstallPath = ExtractedDirectory(x),
                        Dependencies = x.GetCompatiblePackageDependencies(Extensions.CurrentFramework).Select(y => y.Id)
                            .ToArray()
                    }).ToArray(),
                    Frameworks = _contexts.Select(x =>
                            new RuntimeBuilder.FrameworkBuilder() {InstallDirectory = x.InstallPath, Provided = x.Name})
                        .ToArray(),
                    Plugins = orderedPlugins.Select(x => new RuntimeBuilder.PluginBuilder()
                    {
                        Package = x.Key.Id,
                        PluginType = x.Value.AssemblyQualifiedName
                    }).ToArray()
                };
                using (var stream = new MemoryStream())
                {
                    var formatter = new BinaryFormatter();
                    formatter.Serialize(stream, builder);
                    psi.Environment[RuntimeEnvKey] = Convert.ToBase64String(stream.ToArray());
                }
            }

            #endregion


            _log.Debug($"Environment[{RuntimeEnvKey}] = {psi.Environment[RuntimeEnvKey]}");

            psi.UseShellExecute = false;
            psi.FileName = finalExePath;
            return psi;
        }

        public const string RuntimeEnvKey = "Torch.Runtime";

        private static void FillProbePath(string probePath, IEnumerable<string> sources)
        {
            var exists = new HashSet<string>();
            foreach (var entry in sources)
            {
                IEnumerable<string> files = Directory.Exists(entry)
                    ? Directory.EnumerateFiles(entry, "*", SearchOption.AllDirectories)
                    : new[] {entry};
                foreach (var file in files)
                {
                    if (Directory.Exists(file))
                        continue;
                    var name = Path.GetFileName(file);
                    if (!exists.Add(name))
                        continue;

                    // ReSharper disable once AssignNullToNotNullAttribute
                    var dest = Path.Combine(probePath, name);
                    var destDir = Path.GetDirectoryName(dest);
                    if (destDir != null && !Directory.Exists(destDir))
                        Directory.CreateDirectory(destDir);
                    Extensions.CopySmart(file, dest);
                }
            }

            foreach (var file in Directory.EnumerateFiles(probePath))
            {
                if (exists.Contains(Path.GetFileName(file)))
                    continue;
                if (Directory.Exists(file))
                    Directory.Delete(file, true);
                else
                    File.Delete(file);
            }
        }

        private const string HASH_FILE = "hash.txt";
    }
}