﻿using System.Collections.Generic;
using Mono.Cecil;
using Torch.Core.Plugin;

namespace Torch.Loader.Plugins.Patching
{
    public interface IAssemblyReplacer : IAssemblyTransformer
    {
        void Init(IAssemblyResolver implResolver);
    }
}