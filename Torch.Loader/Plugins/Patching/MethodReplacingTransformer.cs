﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Mono.Cecil;
using Mono.Cecil.Cil;

namespace Torch.Loader.Plugins.Patching
{
    public abstract class MethodReplacingTransformer : IAssemblyReplacer
    {
        private IAssemblyResolver _resolver;
        private ReaderParameters _reader;
        private readonly Dictionary<string, ModuleDefinition> _modules = new Dictionary<string, ModuleDefinition>();

        public void Init(IAssemblyResolver implResolver)
        {
            _resolver = implResolver;
            _reader = new ReaderParameters() {AssemblyResolver = implResolver};
        }

        protected ModuleDefinition ResolveModule(string name)
        {
            if (_modules.TryGetValue(name, out var res))
                return res;
            var resolved = _resolver.Resolve(new AssemblyNameDefinition(name, new Version()), _reader);
            return _modules[name] = resolved.MainModule;
        }

        protected void SuffixMethod(MethodDefinition targetMethod, string replacementAssemblyName, string replacementTypeName, string replacementMethodName)
        {
            Debug.Assert(targetMethod.Body.ExceptionHandlers.Count == 0, "Support exceptions asap");

            var proc = targetMethod.Body.GetILProcessor();
            var jmp = FindMethod(targetMethod, replacementAssemblyName, replacementTypeName, replacementMethodName);
            var remap = new Dictionary<Instruction, Instruction>();
            foreach (var ret in targetMethod.Body.Instructions.Where(x => x.OpCode == OpCodes.Ret).ToArray())
            {
                Instruction first = null;
                if (!proc.Body.Method.IsStatic)
                    proc.InsertBefore(ret, first = proc.Create(OpCodes.Ldarg_0));
                for (var i = 0; i < proc.Body.Method.Parameters.Count; i++)
                {
                    var insn = proc.Create(OpCodes.Ldarg, i + (proc.Body.Method.IsStatic ? 0 : 1));
                    if (first == null)
                        first = insn;
                    proc.InsertBefore(ret, insn);
                }

                {
                    var insn = proc.Create(OpCodes.Call, proc.Body.Method.DeclaringType.Module.ImportReference(jmp));
                    if (first == null)
                        first = insn;
                    proc.InsertBefore(ret, insn);
                }
                remap.Add(ret, first);
            }

            foreach (var i in targetMethod.Body.Instructions)
            {
                switch (i.Operand)
                {
                    case Instruction[] ia:
                    {
                        for (var j = 0; j < ia.Length; j++)
                            if (remap.TryGetValue(ia[j], out var repl))
                                ia[j] = repl;
                        break;
                    }
                    case Instruction test:
                    {
                        if (remap.TryGetValue(test, out var repl))
                            i.Operand = repl;
                        break;
                    }
                }
            }
        }

        private MethodDefinition FindMethod(MethodDefinition targetMethod, string replacementAssemblyName, string replacementTypeName, string replacementMethodName)
        {
            string[] matchParameters = (targetMethod.IsStatic ? new string[0] : new[] {targetMethod.DeclaringType.ToString()})
                .Concat(targetMethod.Parameters.Select(x => x.ParameterType.ToString())).ToArray();

            var replacementAssembly = ResolveModule(replacementAssemblyName);
            if (replacementAssembly == null)
                throw new InvalidOperationException("Failed to resolve module: " + replacementAssemblyName);
            var replacementType = replacementAssembly.GetType(replacementTypeName);
            if (replacementType == null)
                throw new InvalidOperationException("Failed to resolve type " + replacementTypeName);
            var replacementMethod = replacementType.Methods.First(testMethod =>
            {
                return testMethod.Name == replacementMethodName && testMethod.ReturnType.ToString().Equals(targetMethod.ReturnType.ToString()) &&
                       testMethod.Parameters.Select(y => y.ParameterType.ToString()).SequenceEqual(matchParameters);
            });
            return replacementMethod;
        }

        protected void ReplaceMethod(MethodDefinition targetMethod, string replacementAssemblyName, string replacementTypeName, string replacementMethodName)
        {
            var body = targetMethod.Body;
            body.Instructions.Clear();
            body.Variables.Clear();
            body.ExceptionHandlers.Clear();

            var proc = body.GetILProcessor();
            EmitCall(proc, FindMethod(targetMethod, replacementAssemblyName, replacementTypeName, replacementMethodName), proc.Append);
            proc.Append(proc.Create(OpCodes.Ret));
        }

        private static void EmitCall(ILProcessor proc, MethodDefinition target, Action<Instruction> inserter)
        {
            if (!proc.Body.Method.IsStatic)
                inserter(proc.Create(OpCodes.Ldarg_0));
            for (var i = 0; i < proc.Body.Method.Parameters.Count; i++)
                inserter(proc.Create(OpCodes.Ldarg, i + (proc.Body.Method.IsStatic ? 0 : 1)));
            inserter(proc.Create(OpCodes.Call, proc.Body.Method.DeclaringType.Module.ImportReference(target)));
        }

        public abstract void TransformEarly(ModuleDefinition info);
        public abstract void TransformLate(ModuleDefinition info);
    }
}