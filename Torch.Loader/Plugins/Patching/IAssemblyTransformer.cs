﻿using Mono.Cecil;

namespace Torch.Loader.Plugins.Patching
{
    public interface IAssemblyTransformer
    {
        /// <summary>
        /// Called before all dependents get a chance to modify the module.
        /// </summary>
        /// <param name="info">Module to modify</param>
        void TransformEarly(ModuleDefinition info);
        
        /// <summary>
        /// Called after all dependents have had a chance to modify the module.
        /// </summary>
        /// <param name="info">Module to modify</param>
        void TransformLate(ModuleDefinition info);
    }
}