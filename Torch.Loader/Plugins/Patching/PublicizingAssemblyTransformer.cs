﻿using System.Collections.Generic;
using Mono.Cecil;

namespace Torch.Loader.Plugins.Patching
{
    public abstract class PublicizingAssemblyTransformer : IAssemblyTransformer
    {
        private readonly HashSet<string> _makePublic;
        
        protected PublicizingAssemblyTransformer(params string[] makePublic)
        {
            _makePublic = new HashSet<string>(makePublic);
        }
        
        public void TransformEarly(ModuleDefinition info)
        {
            foreach (var type in info.GetTypes())
                TransformType(type);
        }

        public void TransformLate(ModuleDefinition info)
        {
        }

        private void TransformType(TypeDefinition type)
        {
            foreach (var nest in type.NestedTypes)
                TransformType(nest);

            if (_makePublic.Contains(type.FullName))
                MakePublic(type);

            bool child = false;
            foreach (var k in _makePublic)
                if (k.StartsWith(type.FullName + "#"))
                {
                    child = true;
                    break;
                }

            if (!child)
                return;

            foreach (var field in type.Fields)
                if (_makePublic.Contains(type.FullName + "#" + field.FullName))
                    MakePublic(field);

            foreach (var method in type.Methods)
                if (_makePublic.Contains(type.FullName + "#" + method.Name))
                    MakePublic(method);
            
        }
        
        // ReSharper disable once MemberCanBePrivate.Global
        public static void MakePublic(FieldDefinition field)
        {
            field.Attributes = (field.Attributes & ~FieldAttributes.FieldAccessMask) | FieldAttributes.Public;
        }

        // ReSharper disable once MemberCanBePrivate.Global
        public static void MakePublic(MethodDefinition method)
        {
            method.Attributes = (method.Attributes & ~MethodAttributes.MemberAccessMask) | MethodAttributes.Public;
        }

        // ReSharper disable once MemberCanBePrivate.Global
        public static void MakePublic(TypeDefinition type)
        {
            if (type.IsNested)
                type.Attributes = (type.Attributes & ~TypeAttributes.VisibilityMask) | TypeAttributes.NestedPublic;
            else
                type.Attributes = (type.Attributes & ~TypeAttributes.VisibilityMask) | TypeAttributes.Public;
        }
    }
}