﻿using System.Collections.Generic;
using NuGet;

namespace Torch.Loader.Plugins.Framework
{
    /// <summary>
    /// Provides the assemblies related to a framework
    /// </summary>
    public interface IFrameworkProvider
    {
        /// <summary>
        /// The package this framework provides
        /// </summary>
        string ProvidedPackage { get; }

        /// <summary>
        /// Dependencies for the package this framework provides
        /// </summary>
        /// <param name="ctx">framework context</param>
        /// <returns>List of dependencies</returns>
        List<PackageDependency> Dependencies(FrameworkContext ctx);

        /// <summary>
        /// Determines the version of the given framework context
        /// </summary>
        /// <param name="ctx">framework context</param>
        /// <returns>The version</returns>
        SemanticVersion Version(FrameworkContext ctx);

        /// <summary>
        /// Determines all assemblies related to the given framework context
        /// </summary>
        /// <param name="ctx">framework context</param>
        /// <returns>Related assemblies</returns>
        IEnumerable<FrameworkAssembly> Assemblies(FrameworkContext ctx);
    }
}