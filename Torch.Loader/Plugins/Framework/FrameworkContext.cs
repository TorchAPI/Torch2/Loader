﻿using System;
using System.Collections.Generic;
using System.IO;
using NuGet;

namespace Torch.Loader.Plugins.Framework
{
    public class FrameworkContext
    {
        private readonly IFrameworkProvider _provider;

        public string Name { get; }

        public string InstallPath { get; }

        // ReSharper disable once SuggestBaseTypeForParameter this ctor should only be used for managed frameworks
        public FrameworkContext(IManagedFrameworkProvider provider, string frameworkInstallDir)
        {
            _provider = provider;
            Name = provider.ProvidedPackage;
            InstallPath = frameworkInstallDir;
        }

        public FrameworkContext(IUnmanagedFrameworkProvider provider)
        {
            _provider = provider;
            Name = provider.ProvidedPackage;
            InstallPath = provider.InstallDirectory;
        }

        public IEnumerable<PackageDependency> Dependencies => _provider.Dependencies(this);
        
        public Type Provider => _provider.GetType();

        public bool IsManaged => _provider is IManagedFrameworkProvider;

        public bool NeedsUpdate => (_provider as IManagedFrameworkProvider)?.NeedsUpdate(this) ?? false;

        public void Update()
        {
            if (!(_provider is IManagedFrameworkProvider provider))
                throw new InvalidOperationException("Can't use the update function on an unmanaged framework");
            
            provider.Update(this);
        }

        public SemanticVersion Version => _provider.Version(this);

        public IEnumerable<FrameworkAssembly> Assemblies => _provider.Assemblies(this);
    }
}