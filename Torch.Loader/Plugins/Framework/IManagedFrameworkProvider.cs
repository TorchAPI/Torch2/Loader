﻿namespace Torch.Loader.Plugins.Framework
{
    /// <summary>
    /// Framework provider that manages itself, with updating functionality.
    /// </summary>
    /// <inheritdoc cref="Torch.Loader.Plugins.Framework.IFrameworkProvider"/>
    public interface IManagedFrameworkProvider : IFrameworkProvider
    {
        bool NeedsUpdate(FrameworkContext ctx);

        void Update(FrameworkContext ctx);
    }
}