﻿namespace Torch.Loader.Plugins.Framework
{
    /// <summary>
    /// Unmanaged framework provider, that provides a view into a framework installed elsewhere.
    /// </summary>
    /// <inheritdoc cref="Torch.Loader.Plugins.Framework.IFrameworkProvider"/>
    public interface IUnmanagedFrameworkProvider : IFrameworkProvider
    {
        /// <summary>
        /// The directory the framework is installed at.
        /// </summary>
        string InstallDirectory { get; }
    }
}