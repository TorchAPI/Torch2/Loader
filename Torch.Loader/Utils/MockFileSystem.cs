﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Versioning;
using NuGet;

namespace Torch.Loader.Utils
{
    public class MockFileSystem : IFileSystem
    {
        private readonly Dictionary<string, byte[]> _files = new Dictionary<string, byte[]>();
        
        public void DeleteDirectory(string path, bool recursive)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetFiles(string path, string filter, bool recursive)
        {
            return _files.Keys;
        }

        public IEnumerable<string> GetDirectories(string path)
        {
            return _files.Keys.Select(Path.GetDirectoryName).Distinct();
        }

        public string GetFullPath(string path)
        {
            return path;
        }

        public void DeleteFile(string path)
        {
            _files.Remove(path);
        }

        public void DeleteFiles(IEnumerable<IPackageFile> files, string rootDir)
        {
            foreach (var k in files)
                _files.Remove(Path.Combine(rootDir, k.Path));
        }

        public bool FileExists(string path)
        {
            return _files.ContainsKey(path);
        }

        public bool DirectoryExists(string path)
        {
            return _files.Keys.Any(x => Path.GetDirectoryName(x) == path);
        }

        public void AddFile(string path, Stream stream)
        {
            var tmp = new MemoryStream();
            stream.CopyTo(tmp);
            _files[path] = tmp.ToArray();
        }

        public void AddFile(string path, Action<Stream> writeToStream)
        {
            var tmp = new MemoryStream();
            writeToStream(tmp);
            _files[path] = tmp.ToArray();
        }

        public void AddFiles(IEnumerable<IPackageFile> files, string rootDir)
        {
            foreach (var f in files)
                AddFile(Path.Combine(rootDir,f.Path), f.GetStream());
        }

        public void MakeFileWritable(string path)
        {
        }

        public void MoveFile(string source, string destination)
        {
            _files[destination] = _files[source];
            _files.Remove(source);
        }

        public Stream CreateFile(string path)
        {
            throw new NotImplementedException();
        }

        public Stream OpenFile(string path)
        {
            return new MemoryStream(_files[path]);
        }

        public DateTimeOffset GetLastModified(string path)
        {
            return DateTimeOffset.Now;
        }

        public DateTimeOffset GetCreated(string path)
        {
            return DateTimeOffset.Now;
        }

        public DateTimeOffset GetLastAccessed(string path)
        {
            return DateTimeOffset.Now;
        }

        public ILogger Logger { get; set; }
        public string Root { get; }
    }
}