﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Versioning;
using System.Text;
using NuGet;

namespace Torch.Loader.Utils
{
    public static class Extensions
    {
        private static readonly MethodInfo _installWithFramework;
        public static readonly FrameworkName CurrentFramework;

        static Extensions()
        {
            _installWithFramework = typeof(PackageManager).GetMethod("InstallPackage",
                BindingFlags.NonPublic | BindingFlags.Instance, null, new[]
                {
                    typeof(IPackage), typeof(FrameworkName), typeof(bool), typeof(bool), typeof(bool)
                }, null);
            CurrentFramework = new FrameworkName(AppDomain.CurrentDomain.SetupInformation.TargetFrameworkName);
        }

        public static IEnumerable<IPackage> Dependencies(this IPackageRepository repo, IEnumerable<IPackage> packages)
        {
            var result = new Dictionary<string, IPackage>();
            var explore = new Stack<IPackage>(packages);
            while (explore.Count > 0)
            {
                var test = explore.Pop();
                if (result.TryGetValue(test.Id, out var curr) && curr.Version > test.Version)
                    continue;
                result[test.Id] = test;
                foreach (var tmp in test.GetCompatiblePackageDependencies(CurrentFramework))
                    explore.Push(repo.FindPackages(tmp.Id, tmp.VersionSpec, true, true)
                        .OrderByDescending(x => x.Version).First());
            }

            return result.Values;
        }

        public static void InstallPackageCurrentFramework(this PackageManager manager, IPackage package,
            bool ignoreDependencies = false, bool allowPrereleaseVersions = true, bool ignoreWalkInfo = false)
        {
            InstallPackage(manager, package, CurrentFramework, ignoreDependencies,
                allowPrereleaseVersions, ignoreWalkInfo);
        }

        public static void InstallPackage(this PackageManager manager, IPackage package, FrameworkName targetFramework,
            bool ignoreDependencies, bool allowPrereleaseVersions, bool ignoreWalkInfo = false)
        {
            _installWithFramework.Invoke(manager,
                new object[] {package, targetFramework, ignoreDependencies, allowPrereleaseVersions, ignoreWalkInfo});
        }

        public static bool IsBinaryFile(this IPackageFile file)
        {
            return file.Path.EndsWith(".dll", StringComparison.OrdinalIgnoreCase) ||
                   file.Path.EndsWith(".exe", StringComparison.OrdinalIgnoreCase);
        }

        public static bool IsMixin(this IPackageFile f)
        {
            return f.IsBinaryFile() &&
                   f.Path.Replace("/", "\\").StartsWith("lib\\mixin\\", StringComparison.OrdinalIgnoreCase);
        }

        public static bool IsPatcher(this IPackageFile f)
        {
            return f.IsBinaryFile() &&
                   f.Path.Replace("/", "\\").StartsWith("lib\\patch\\", StringComparison.OrdinalIgnoreCase);
        }

        public static string AsHash(byte[] data)
        {
            var sb = new StringBuilder(data.Length * 2);
            foreach (var b in data)
                sb.Append(GetHexValue(b >> 4)).Append(GetHexValue(b & 15));
            return sb.ToString();
        }

        public static bool EqualsFuzzy(this Type a, Type b)
        {
            return a.FullName == b.FullName;
        }

        public static void CopySmart(string src, string dst)
        {
            if (File.Exists(dst))
            {
                var srcI = new FileInfo(src);
                var dstI = new FileInfo(dst);
                if (srcI.Length == dstI.Length && srcI.LastWriteTime == dstI.LastWriteTime)
                    return;
                File.Delete(dst);
            }
            File.Copy(src, dst);
        }

        public static bool IsAssignableFromFuzzy(this Type a, Type b)
        {
            foreach (var inter in b.GetInterfaces())
                if (inter.EqualsFuzzy(a))
                    return true;
            var type = b;
            while (type != null)
            {
                if (type.EqualsFuzzy(a))
                    return true;
                type = type.BaseType;
            }

            return false;
        }

        private static char GetHexValue(int i)
        {
            if (i < 10)
                return (char) (i + 48);
            return (char) (i - 10 + 65);
        }
    }
}