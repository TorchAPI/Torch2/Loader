﻿using System;
using System.Collections.Generic;
using System.Linq;
using NuGet;
using Torch.Core.Util;
using CustomAttributeExtensions = System.Reflection.CustomAttributeExtensions;

namespace Torch.Loader.Utils
{
    public static class PackageDag
    {
        public static IEnumerable<KeyValuePair<IPackage, Type>> SelectOrdered(
            this ICollection<KeyValuePair<IPackage, Type>> src)
        {
            var pkgDependents = SelectPackageDependents(src);
            var annoDependents = SelectAnnotatedDependents(src);

            return src.SortDag((x) => pkgDependents(x).Concat(annoDependents(x)), SelectPriority());
        }

        private static Func<KeyValuePair<IPackage, Type>, IEnumerable<KeyValuePair<IPackage, Type>>>
            SelectPackageDependents(ICollection<KeyValuePair<IPackage, Type>> roots)
        {
            return (package) => package.Key.GetCompatiblePackageDependencies(Extensions.CurrentFramework).SelectMany(
                dependency => roots.Where(root => root.Key.Id == dependency.Id && dependency.VersionSpec.Satisfies(root.Key.Version)));
        }

        private static Func<KeyValuePair<IPackage, Type>, IEnumerable<KeyValuePair<IPackage, Type>>>
            SelectAnnotatedDependents(ICollection<KeyValuePair<IPackage, Type>> roots)
        {
            return (package) =>
            {
                var coll = new List<KeyValuePair<IPackage, Type>>();
                foreach (var attr in package.Value.GetCustomAttributesData())
                {
                    if (!attr.AttributeType.EqualsFuzzy(typeof(DependencyAttribute)))
                        continue;
                    foreach (var arg in attr.ConstructorArguments)
                    {
                        if (!arg.ArgumentType.EqualsFuzzy(typeof(Type))) continue;
                        foreach (var result in roots)
                            if (result.Value.EqualsFuzzy((Type) arg.Value))
                                coll.Add(result);
                    }
                }

                return coll;
            };
        }

        private static Func<KeyValuePair<IPackage, Type>, float> SelectPriority()
        {
            return (x) =>
            {
                foreach (var attr in x.Value.GetCustomAttributesData())
                {
                    if (!attr.AttributeType.EqualsFuzzy(typeof(PriorityAttribute)))
                        continue;
                    if (attr.NamedArguments == null) continue;
                    foreach (var nam in attr.NamedArguments)
                        if (nam.MemberName == nameof(PriorityAttribute.Priority) &&
                            nam.TypedValue.ArgumentType == typeof(float))
                            return (float) nam.TypedValue.Value;
                }

                return 0f;
            };
        }
    }
}