﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NuGet;

namespace Torch.Loader.Utils
{
    public static class PackageResolver
    {
        private static readonly HashSet<string> _resolvePaths = new HashSet<string>();

        public static void Add(string extracted, IPackage pkg)
        {
            if (!VersionUtility.TryGetCompatibleItems(Extensions.CurrentFramework, pkg.GetFiles(), out var files))
                throw new InvalidOperationException($"No compatible items for {pkg.Id}/{pkg.Version}");

            if (!Directory.Exists(extracted))
                throw new InvalidOperationException($"Could not find extracted data for {pkg.Id} {pkg.Version}");
            foreach (var file in files.Where(x => x.IsBinaryFile()))
            {
                var abs = Path.Combine(extracted, file.Path);
                AddPath(Path.GetDirectoryName(abs));
            }
        }

        public static void AddPath(string extracted, IPackage pkg, string directory)
        {
            if (!Directory.Exists(extracted))
                throw new InvalidOperationException($"Could not find extracted data for {pkg.Id} {pkg.Version}");
            foreach (var file in pkg.GetFiles()
                .Where(x => directory == null || x.Path.StartsWith(directory, StringComparison.OrdinalIgnoreCase))
                .Where(x => x.IsBinaryFile()))
            {
                var abs = Path.Combine(extracted, file.Path);
                AddPath(Path.GetDirectoryName(abs));
            }
        }

        public static void AddPath(string dir)
        {
            if (_resolvePaths.Add(dir))
            {
                // Really can't figure out a better way to do this.
#pragma warning disable 618
                AppDomain.CurrentDomain.AppendPrivatePath(dir);
#pragma warning restore 618
            }
        }
    }
}