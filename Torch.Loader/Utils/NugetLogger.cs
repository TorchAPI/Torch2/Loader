﻿using System;
using NLog;
using NuGet;

namespace Torch.Loader.Utils
{
    public class NugetLogger : NuGet.ILogger
    {
        private static readonly NLog.ILogger _log = LogManager.GetCurrentClassLogger();

        public static readonly NugetLogger Instance = new NugetLogger();
        
        public FileConflictResolution ResolveFileConflict(string message)
        {
            return FileConflictResolution.OverwriteAll;
        }

        public void Log(MessageLevel level, string message, params object[] args)
        {
            switch (level)
            {
                case MessageLevel.Info:
                    _log.Info(message, args);
                    break;
                case MessageLevel.Warning:
                    _log.Warn(message, args);
                    break;
                case MessageLevel.Debug:
                    _log.Debug(message, args);
                    break;
                case MessageLevel.Error:
                    _log.Error(message, args);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(level), level, null);
            }
        }
    }
}