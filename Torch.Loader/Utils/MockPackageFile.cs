﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Versioning;
using NuGet;

namespace Torch.Loader.Utils
{
    public class MockPackageFile : IPackageFile
    {
        private readonly Func<Stream> _streamFactory;
        private string _path;

        public MockPackageFile(Func<Stream> streamFactory)
        {
            _streamFactory = streamFactory;
        }

        public MockPackageFile(byte[] data)
        {
            _streamFactory = () => new MemoryStream(data);
        }

        public MockPackageFile(string content)
        {
            var data = new MemoryStream(content.Length * 2);
            using (var writer = new StreamWriter(data))
                writer.Write(content);
            var rawData = data.ToArray();
            _streamFactory = () => new MemoryStream(rawData);
        }

        public string Path
        {
            get => _path;
            set
            {
                if (string.Compare(_path, value, StringComparison.OrdinalIgnoreCase) == 0)
                    return;
                _path = value;
                TargetFramework = VersionUtility.ParseFrameworkNameFromFilePath(_path, out var effectivePath);
                EffectivePath = effectivePath;
            }
        }

        public string EffectivePath { get; private set; }

        public FrameworkName TargetFramework { get; private set; }

        public IEnumerable<FrameworkName> SupportedFrameworks
        {
            get
            {
                if (TargetFramework != null)
                    yield return TargetFramework;
            }
        }

        public Stream GetStream() => _streamFactory();
    }
}