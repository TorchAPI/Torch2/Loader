﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Torch.Loader.Utils
{
    public static class AssemblyRedirect
    {
        public static IReadOnlyList<string> RedirectAssemblies(IEnumerable<string> input, bool excludeLoaded)
        {
            var redirected = new Dictionary<string, KeyValuePair<string, Version>>();
            foreach (var k in input)
            {
                var asmName = Path.GetFileNameWithoutExtension(k) ??
                              throw new InvalidOperationException($"Failed to get filename for {k}");
                var fileInfo = FileVersionInfo.GetVersionInfo(k);
                Version.TryParse(fileInfo.FileVersion, out var cver);
                if (!redirected.TryGetValue(asmName, out var casm) || casm.Value == null ||
                    (cver != null && casm.Value < cver))
                    redirected[asmName] = new KeyValuePair<string, Version>(k, cver);
            }

            // ReSharper disable once InvertIf
            if (excludeLoaded)
            {
                var preloadedAssemblies = AppDomain.CurrentDomain.GetAssemblies();
                foreach (var p in preloadedAssemblies)
                    redirected.Remove(p.GetName().Name);
            }

            return new ReadOnlyCollection<string>(redirected.Values.Select(x => x.Key).ToList());
        }


        public static IReadOnlyList<KeyValuePair<TU, string>> RedirectAssemblies<TU>(
            IEnumerable<KeyValuePair<TU, string>> input, bool excludeLoaded)
        {
            var redirected = new Dictionary<string, KeyValuePair<KeyValuePair<TU, string>, Version>>();
            foreach (var kv in input)
            {
                var k = kv.Value;
                var asmName = Path.GetFileNameWithoutExtension(k) ??
                              throw new InvalidOperationException($"Failed to get filename for {k}");
                var fileInfo = FileVersionInfo.GetVersionInfo(k);
                Version.TryParse(fileInfo.FileVersion, out var cver);
                if (!redirected.TryGetValue(asmName, out var casm) || casm.Value == null ||
                    (cver != null && casm.Value < cver))
                    redirected[asmName] =
                        new KeyValuePair<KeyValuePair<TU, string>, Version>(kv,
                            cver);
            }

            // ReSharper disable once InvertIf
            if (excludeLoaded)
            {
                var preloadedAssemblies = AppDomain.CurrentDomain.GetAssemblies();
                foreach (var p in preloadedAssemblies)
                    redirected.Remove(p.GetName().Name);
            }
            
            return new ReadOnlyCollection<KeyValuePair<TU, string>>(redirected.Values.Select(x => x.Key).ToList());
        }
    }
}