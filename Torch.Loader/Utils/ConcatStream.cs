﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Torch.Loader.Utils
{
    public class ConcatStream : Stream
    {
        private readonly Queue<Stream> _streams = new Queue<Stream>();

        public void Add(Stream stream)
        {
            _streams.Enqueue(stream);
        }
        
        public override void Flush()
        {
            throw new System.NotImplementedException();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new System.NotImplementedException();
        }

        public override void SetLength(long value)
        {
            throw new System.NotImplementedException();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            int bytesRead = 0;
            while (_streams.Count > 0 && bytesRead < count)
            {
                var self = _streams.Peek().Read(buffer, offset + bytesRead, count - bytesRead);
                bytesRead += self;
                if (self != 0)
                    return bytesRead;
                _streams.Dequeue().Dispose();
            }

            return bytesRead;
        }

        public override void Close()
        {
            while (_streams.Count > 0)
                _streams.Dequeue().Dispose();
            base.Close();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new System.NotImplementedException();
        }

        public override bool CanRead => true;
        public override bool CanSeek => false;
        public override bool CanWrite => false;
        public override long Length => throw new NotImplementedException();

        public override long Position
        {
            get => throw new NotImplementedException();
            set => throw new NotImplementedException();
        }
    }
}